"""shelterproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from rest_framework.authtoken import views as RFviews

from shelter.views import AnimalViewSetReadOnly, AnimalViewSet
from news.views import NewsViewSetReadOnly, NewsViewSet
from accounts.views import UserGroupRetrieveView

router = routers.DefaultRouter()

router.register(r'news', NewsViewSetReadOnly)
router.register(r'update-news', NewsViewSet)

router.register(r'animals', AnimalViewSetReadOnly)
router.register(r'update-animals', AnimalViewSet)

router.register(r'usergroups', UserGroupRetrieveView, base_name='User')

urlpatterns = [
    path('shelteradmin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-token-auth/', RFviews.obtain_auth_token),
    path('api-auth/', include('rest_framework.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

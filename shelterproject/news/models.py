from django.db import models
from django.utils.text import slugify
from shelter.validators import image_restriction

class News(models.Model):

    class Meta:
        verbose_name_plural = "news"

    short_title = models.CharField(max_length=50, unique=True)
    full_title = models.CharField(max_length=200)
    content = models.TextField()
    slug = models.SlugField(blank=True)

    date_created = models.DateTimeField(blank=True, auto_now_add=True)
    date_of_event = models.DateField(blank=True, null=True)
    document_link = models.CharField(max_length=200, blank=True, default="")
    social_link = models.CharField(max_length=200, blank=True, default="")
    image = models.ImageField(validators=[image_restriction,],upload_to='uploads/news/', blank=True, null=True)

    def __str__(self):
        return self.short_title + " - " + str(self.date_created)[:11]

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.short_title)

        super(News, self).save(*args, **kwargs)

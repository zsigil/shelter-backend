from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions

from .models import News
from .serializers import NewsSerializer

class NewsViewSetReadOnly(viewsets.ReadOnlyModelViewSet):
    queryset = News.objects.all().order_by('-date_created')
    serializer_class = NewsSerializer

class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all().order_by('-date_created')
    serializer_class = NewsSerializer
    permission_classes = (DjangoModelPermissions,)

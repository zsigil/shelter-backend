from rest_framework import viewsets
from rest_framework.permissions import DjangoModelPermissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import action

from django.shortcuts import get_object_or_404

from .serializers import AnimalSerializer
from .models import Animal

from django.conf import settings
User = settings.AUTH_USER_MODEL

class AnimalViewSetReadOnly(viewsets.ReadOnlyModelViewSet):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer

class AnimalViewSet(viewsets.ModelViewSet):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer
    permission_classes = (DjangoModelPermissions,)

    def create(self, request, *args, **kwargs):
        """
        DEMO version allows posting limited number of animals"
        """
        if Animal.objects.all().count()>10:
            return Response('Demo version. Must delete before adding new animal')

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=True)
    def make_animal_of_week(self, request, *args, **kwargs):
        #check permission
        user = request.user
        if user.has_perm('shelter.add_animal'):
            """
            reset first, only 1 can be animal of week
            """
            previous_animal_of_week = Animal.objects.filter(animal_of_week=True).first()
            if previous_animal_of_week:
                previous_animal_of_week.animal_of_week = False
                previous_animal_of_week.save()

            """
            set animal of the week
            """
            animal = self.get_object()
            animal.animal_of_week = True
            animal.save()
            return Response('Kijelölés sikerült.')

        else:
            return Response('Nincs jogosultság')

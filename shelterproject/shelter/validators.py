from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from datetime import date

def thumbnail_restriction(image):
    # image_width, image_height = get_image_dimensions(image)
    # if image_width >= 500 or image_height >= 500:
    if image.size>30*1024:
        raise ValidationError('Image size needs to be less than 30kb')

def image_restriction(image):
    # image_width, image_height = get_image_dimensions(image)
    # if image_width >= 1000 or image_height >= 1000:
    if image.size>200*1024:
        raise ValidationError('Image size needs to be less than 200kb')

def futuredate_check(certain_date):
    today = date.today()
    if certain_date > today:
        raise ValidationError('Date is in future')

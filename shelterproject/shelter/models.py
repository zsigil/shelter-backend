from django.db import models
from .validators import (
    image_restriction,
    thumbnail_restriction,
    futuredate_check)
from datetime import date

import os
from django.dispatch import receiver


class Animal(models.Model):
    #required fields
    shelterID = models.CharField(max_length=100, unique=True, blank=False)
    name = models.CharField(max_length=100, blank=False)
    species = models.CharField(max_length=100, default="eb")
    gender = models.CharField(max_length=100, blank=False)
    age = models.CharField(max_length=100, blank=False)

    #optional fields
    breed = models.CharField(max_length=100, null=True, blank=True)
    size = models.CharField(max_length=100, null=True, blank=True)
    date_of_arrival = models.DateField(validators=[futuredate_check], default=date.today)
    date_of_adoption = models.DateField(validators=[futuredate_check], null=True, blank=True)

    puppy = models.BooleanField(default=False)
    adoptable = models.BooleanField(default=False)

    colorcode = models.CharField(max_length=20, null=True, blank=True)
    social_link_string = models.CharField(max_length=200, null=True, blank=True)
    documents_link_string = models.CharField(max_length=200,  null=True, blank=True)
    other = models.TextField(null=True, blank=True)
    story = models.TextField(null=True, blank=True)


    #images
    thumbnailImage = models.ImageField(validators=[thumbnail_restriction],upload_to='uploads/', default="default.jpg")
    mainImage = models.ImageField(validators=[image_restriction],upload_to='uploads/', default="default.jpg")
    image2 = models.ImageField(validators=[image_restriction],upload_to='uploads/', blank=True)
    image3 = models.ImageField(validators=[image_restriction],upload_to='uploads/', blank=True)

    #extra properties
    animal_of_week = models.BooleanField(default=False)

    @property
    def adopted(self):
        return True if self.date_of_adoption else False

    def __str__(self):
        return self.name + " " + self.shelterID

    def save(self, *args, **kwargs):
        if self.date_of_adoption:
            self.adoptable = False
            if self.date_of_adoption < self.date_of_arrival:
                raise ValueError('Cannot be adopted before arrival')

        super(Animal, self).save(*args, **kwargs)

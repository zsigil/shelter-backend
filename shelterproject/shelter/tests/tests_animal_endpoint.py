from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.test import APITestCase

from shelter.models import Animal


class AnimalCreateTests(APITestCase):
    """
    Ensure that /api/animals/ endpoint is readonly
    """
    def setUp(self):
        self.client = APIClient()

    def test_create_animal(self):
        url = '/api/animals/'
        data = {'shelterID': '1234', 'age': '3 hó', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        self.assertEqual(Animal.objects.count(), 0)


class AnimalsChangeAndDeleteTests(APITestCase):
    """
    Ensure that /api/animals/ endpoint is readonly
    """
    def setUp(self):
        self.client = APIClient()
        self.animal =Animal.objects.create(shelterID="123", name="Fifi", age="3 hó", gender="kan")

    def test_update_or_delete_animal(self):
        url = '/api/animals/{}/'.format(self.animal.id)
        data = {'name':"Fufu"}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(self.animal.name, 'Fifi')

        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(Animal.objects.count(), 1)

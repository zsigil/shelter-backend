from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.test import APITestCase

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model

from shelter.models import Animal

import time

User = get_user_model();

class UpdateAnimalCreateTests(APITestCase):
    """
    Ensure that users who have the permission 'shelter.add_animal',
    can create animals, but others cannot
    """
    def setUp(self):
        self.client = APIClient()

    def test_create_animal_not_authenticated(self):
        """
        Ensure that unauthenticated users cannot create a new animal object.
        """
        url = '/api/update-animals/'
        data = {'shelterID': '1234', 'age': '3 hó', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Animal.objects.count(), 0)

    def test_create_animal_unauthorized(self):
        """
        Ensure that authenticated users without add_animal permission cannot create a new animal object.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-animals/'
        data = {'shelterID': '1234', 'age': '3 hó', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}

        response = self.client.post(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Animal.objects.count(), 0)

    def test_create_animal_authorized_and_permission(self):
        """
        Ensure that authenticated users with add_animal permission can create a new animal object.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        user.user_permissions.add(Permission.objects.get(codename='add_animal'))
        user.save()
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-animals/'
        data = {'shelterID': '1234', 'age': '3 hó', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}

        response = self.client.post(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Animal.objects.count(), 1)

class UpdateAnimalChangeTests(APITestCase):
    """
    Ensure that users who have the permission 'shelter.change_animal',
    can change animals, but others cannot
    """
    def setUp(self):
        self.client = APIClient()
        self.animal =Animal.objects.create(shelterID="123", name="Fifi", age="3 hó", gender="kan")

    def test_update_animal_not_authenticated(self):
        """
        Ensure that unauthenticated users cannot update animal object.
        """
        url = '/api/update-animals/{}/'.format(self.animal.id)
        data = {'age': '5év', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_animal_unauthorized(self):
        """
        Ensure that authenticated users without change_animal permission cannot update animal.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-animals/{}/'.format(self.animal.id)
        data = {'age': '5 év', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}

        response = self.client.patch(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Animal.objects.count(), 1)

    def test_update_animal_authorized_and_permission(self):
        """
        Ensure that authenticated users with change_animal permission can change animal.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        content_type = ContentType.objects.get_for_model(Animal)
        user.user_permissions.add(Permission.objects.get(codename='change_animal', content_type=content_type))
        user.save()
        self.assertEqual(user.has_perm('shelter.change_animal'),True)
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        self.assertEqual(Animal.objects.count(), 1)
        animal = Animal.objects.get(pk=1)
        self.assertEqual(animal.age, '3 hó')
        url = '/api/update-animals/{}/'.format(self.animal.id)
        data = {'age': '5 év', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}


        response = self.client.patch(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        time.sleep(3)
        animal = Animal.objects.get(pk=1)
        self.assertEqual(animal.age, '5 év')

class UpdateAnimalDeleteTests(APITestCase):
    """
    Ensure that users who have the permission 'shelter.delete_animal',
    can delete animals, but others cannot
    """
    def setUp(self):
        self.client = APIClient()
        self.animal =Animal.objects.create(shelterID="123", name="Fifi", age="3 hó", gender="kan")

    def test_delete_animal_not_authenticated(self):
        """
        Ensure that unauthenticated users cannot delete animal object.
        """
        url = '/api/update-animals/{}/'.format(self.animal.id)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(Animal.objects.count(), 1)

    def test_delete_animal_unauthorized(self):
        """
        Ensure that authenticated users without delete_animal permission cannot delete animal.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-animals/{}/'.format(self.animal.id)

        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Animal.objects.count(), 1)

    def test_delete_animal_authorized_and_permission(self):
        """
        Ensure that authenticated users with delete_animal permission can delete animal.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        content_type = ContentType.objects.get_for_model(Animal)
        user.user_permissions.add(Permission.objects.get(codename='delete_animal', content_type=content_type))
        user.save()
        self.assertEqual(user.has_perm('shelter.delete_animal'),True)

        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-animals/{}/'.format(self.animal.id)

        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        time.sleep(3)
        self.assertEqual(Animal.objects.count(), 0)

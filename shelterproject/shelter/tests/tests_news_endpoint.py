from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.test import APITestCase

from news.models import News


class NewsCreateTests(APITestCase):
    """
    Ensure that /api/news/ endpoint is readonly
    """
    def setUp(self):
        self.client = APIClient()

    def test_create_news(self):
        url = '/api/news/'
        data = {'short_title': 'abc', 'full_title': 'abcdef', 'content': 'abcdefghij'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(News.objects.count(), 0)


class NewsChangeTests(APITestCase):
    """
    Ensure that /api/news/ endpoint is readonly
    """
    def setUp(self):
        self.client = APIClient()
        self.newsitem = News.objects.create(short_title='abc', full_title="abcd", content="abcdefgh")

    def test_update_news(self):

        url = '/api/news/{}/'.format(self.newsitem.id)
        data = {'short_title': 'abcnew', 'full_title': 'abcdef', 'content': 'abcdefghij'}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

class NewsDeleteTests(APITestCase):
    """
    Ensure that /api/news/ endpoint is readonly
    """
    def setUp(self):
        self.client = APIClient()
        self.newsitem = News.objects.create(short_title='abc', full_title="abcd", content="abcdefgh")

    def test_delete_news(self):

        url = '/api/news/{}/'.format(self.newsitem.id)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertEqual(News.objects.count(), 1)

# from rest_framework.test import APIClient
# from rest_framework import status
# from rest_framework.test import APITestCase
#
# from django.contrib.auth.models import Permission
# from django.contrib.contenttypes.models import ContentType
# from django.contrib.auth import get_user_model
#
# from io import BytesIO
# from PIL import Image
# from django.core.files.base import File
#
# from shelter.models import Animal
#
# User = get_user_model();
#
# class ImageValidationTests(APITestCase):
#     """
#     Ensure that thumbnailimages are no more than 30kb, and other images are max. 200kb
#     """
#
#     def setUp(self):
#         self.client = APIClient()
#
#         #user with permission to post
#         self.user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
#         self.user.user_permissions.add(Permission.objects.get(codename='add_animal'))
#         self.user.save()
#         self.token = self.user.auth_token
#         self.tokenauth = 'Token {}'.format(self.token)
#
#         self.url = '/api/update-animals/'
#
#     @staticmethod
#     def get_image(name, ext='png', size=(50, 50), color=(256, 0, 0)):
#         file_obj = BytesIO()
#         image = Image.new("RGBA", size=size, color=color)
#         image.save(file_obj, ext)
#         file_obj.seek(0)
#         return File(file_obj, name=name)
#
#     def test_thumbnailimage_sizeok_post(self):
#         thumbnailImage = self.get_image('image.png', size=(20,20))
#         self.assertTrue(thumbnailImage.size<=30000)
#         data = {'thumbnailImage':thumbnailImage,'shelterID': '1234', 'age': '3 hó', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}
#
#         response = self.client.post(self.url, data, HTTP_AUTHORIZATION=self.tokenauth)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#
#         animal = Animal.objects.get(pk=1)
#
#     def test_thumbnailimage_toobig_post(self):
#         thumbnailImage = self.get_image('image.png', size=(20000,20000))
#         self.assertTrue(thumbnailImage.size>30000)
#         data = {'thumbnailImage':thumbnailImage,'shelterID': '1234', 'age': '3 hó', 'gender': 'szuka', 'name':'Fifi', 'puppy':'true'}
#
#         response = self.client.post(self.url, data, HTTP_AUTHORIZATION=self.tokenauth)
#         self.assertEqual(response.status_code, status.HTTP_201_CREATED)
#         self.assertEqual(Animal.objects.all().count(),1)

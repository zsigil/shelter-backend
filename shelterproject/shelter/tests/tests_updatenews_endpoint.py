from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.test import APITestCase

from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model

from news.models import News

import time

User = get_user_model();

class UpdateNewsCreateTests(APITestCase):
    """
    Ensure that users who have the permission 'news.add_news',
    can create news, but others cannot
    """
    def setUp(self):
        self.client = APIClient()

    def test_create_news_not_authenticated(self):
        """
        Ensure that unauthenticated users cannot create a new news object.
        """
        url = '/api/update-news/'
        data = {'short_title': 'abc', 'full_title': 'abcdef', 'content': 'abcdefghij'}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(News.objects.count(), 0)

    def test_create_news_unauthorized(self):
        """
        Ensure that authenticated users without add_news permission cannot create a new news object.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-news/'
        data = {'short_title': 'abc', 'full_title': 'abcdef', 'content': 'abcdefghij'}

        response = self.client.post(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(News.objects.count(), 0)

    def test_create_news_authorized_and_permission(self):
        """
        Ensure that authenticated users with add_news permission can create a new news object.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        user.user_permissions.add(Permission.objects.get(codename='add_news'))
        user.save()
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-news/'
        data = {'short_title': 'abc', 'full_title': 'abcdef', 'content': 'abcdefghij'}

        response = self.client.post(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(News.objects.count(), 1)


class UpdateNewsChangeTests(APITestCase):
    """
    Ensure that users who have the permission 'news.change_news',
    can change news, but others cannot
    """
    def setUp(self):
        self.client = APIClient()
        self.newsitem = News.objects.create(short_title='abc', full_title="abcd", content="abcdefgh")

    def test_update_news_not_authenticated(self):
        """
        Ensure that unauthenticated users cannot update news object.
        """
        url = '/api/update-news/{}/'.format(self.newsitem.id)
        data = {'short_title': 'abcnew', 'full_title': 'abcdef', 'content': 'abcdefghij'}
        response = self.client.patch(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_update_news_unauthorized(self):
        """
        Ensure that authenticated users without change_news permission cannot update news.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-news/{}/'.format(self.newsitem.id)
        data = {'short_title': 'abcnew', 'full_title': 'abcdef', 'content': 'abcdefghij'}

        response = self.client.patch(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_news_authorized_and_permission(self):
        """
        Ensure that authenticated users with change_news permission can change news.
        """

        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)
        content_type = ContentType.objects.get_for_model(News)
        user.user_permissions.add(Permission.objects.get(codename='change_news', content_type=content_type))
        user.save()
        self.assertEqual(user.has_perm('news.change_news'),True)

        newsitem = News.objects.get(pk=1)
        self.assertEqual(newsitem.short_title, 'abc')
        url = '/api/update-news/{}/'.format(self.newsitem.id)
        data = {'short_title': 'abcnew'}

        response = self.client.patch(url, data, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        time.sleep(3)
        newsitem = News.objects.get(pk=1)
        self.assertEqual(newsitem.short_title, 'abcnew')


class UpdateNewsDeleteTests(APITestCase):
    """
    Ensure that users who have the permission 'news.delete_news',
    can delete news, but others cannot
    """
    def setUp(self):
        self.client = APIClient()
        self.newsitem = News.objects.create(short_title='abc', full_title="abcd", content="abcdefgh")

    def test_delete_news_not_authenticated(self):
        """
        Ensure that unauthenticated users cannot delete news object.
        """
        url = '/api/update-news/{}/'.format(self.newsitem.id)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(News.objects.count(), 1)

    def test_delete_news_unauthorized(self):
        """
        Ensure that authenticated users without delete_news permission cannot delete news.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-news/{}/'.format(self.newsitem.id)

        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(News.objects.count(), 1)

    def test_delete_news_authorized_and_permission(self):
        """
        Ensure that authenticated users with delete_news permission can delete news.
        """
        user = User.objects.create(username="testuser", email="test@test.com", password="secret123")
        content_type = ContentType.objects.get_for_model(News)
        user.user_permissions.add(Permission.objects.get(codename='delete_news', content_type=content_type))
        user.save()
        self.assertEqual(user.has_perm('news.delete_news'),True)
        self.assertEqual(News.objects.count(), 1)
        token = user.auth_token
        tokenauth = 'Token {}'.format(token)

        url = '/api/update-news/{}/'.format(self.newsitem.id)

        response = self.client.delete(url, HTTP_AUTHORIZATION=tokenauth)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        time.sleep(3)
        self.assertEqual(News.objects.count(), 0)

from rest_framework import routers, serializers, viewsets
from .models import Animal

class AnimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Animal
        fields =  [
            'id',
            'shelterID',
            'name',
            'species',
            'gender',
            'age',

            'breed',
            'size',
            'date_of_arrival',
            'date_of_adoption',
            'puppy',
            'adoptable',
            'colorcode',
            'other',
            'story',
            'thumbnailImage',
            'mainImage',
            'image2',
            'image3',
            'animal_of_week',
            'adopted',
            'social_link_string',
            'documents_link_string',
        ]

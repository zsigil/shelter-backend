from rest_framework import serializers
from django.contrib.auth import get_user_model
User = get_user_model()

class UserSerializer(serializers.ModelSerializer):
    allpermissions = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['groups','allpermissions']

    def get_allpermissions(self, obj):
        return obj.get_all_permissions()

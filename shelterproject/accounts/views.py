from rest_framework import generics
from rest_framework import mixins
from rest_framework import viewsets
from .serializers import UserSerializer
from django.contrib.auth import get_user_model
User = get_user_model()

class UserGroupRetrieveView(mixins.ListModelMixin, viewsets.GenericViewSet):

    def get_queryset(self):
        return [self.request.user,]

    serializer_class = UserSerializer

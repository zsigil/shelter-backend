### python-decouple

.env file is needed in main project folder with 'SECRET_KEY' (random string) and 'DEBUG' (boolean) variables


# API endpoints

### GET, POST /api/update-animals/
### PATCH, DELETE /api/update-animals/:id/
### GET, POST /api/update-news/
### PATCH, DELETE /api/update-news/:id/

..* authentication + permissions required
  group member 'Shelter Staff' (group 1): can view(GET)/add(POST)/delete(DELETE)/change(PUT,PATCH) animals/news
  group member 'Helpers' (group 2): can add animal/news

### GET /api/update-animals/:id/make_animal_of_week/

..* authentication needed, 'add_animal' permission required



..* DEMO VERSION: limited number of animals(10) can be uploaded!


### /api/animals/
### /api/animals/:id
### /api/news/
### /api/news/:id


..* readonly for all users (GET, HEAD, OPTIONS methods allowed)

### GET /api/usergroups/


..* only for authenticated users

list of exactly one object containing the group number the user belongs to, and all permissions eg.:

  [
    {"groups":[1],
    "allpermissions":["shelter.change_animal","shelter.add_animal","shelter.v iew_animal","shelter.delete_animal"]
    }
  ]


# authentication-type

token authentication
users can only be created on admin site


### POST /api-token-auth/

logging in with the correct username/password will return token


## image handling

uploaded images are deleted when corresponding instance is deleted!

max 30kb thumbnail (other images max 200kb)
